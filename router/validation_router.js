const express = require('express');
const router = express.Router();
const db  = require('../config/db.config.js');
const { signupValidation, loginValidation } = require('../validation/validation_creation.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


router.post('/register', signupValidation, (req, res, next) => {

    db.query(`SELECT * FROM user WHERE LOWER(mailid) = LOWER(${db.escape(req.body.mailid)});`,(err, result) => {
        if (result.length) {
            return res.status(409).send({ msg: 'This user is already in use!' });
        }else {
                   //username is available
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).send({ msg: err });
                }else {
                            // has hashed pw => add to database
                    db.query(`INSERT INTO user(mailid,password,lastlogin) VALUES (${db.escape( req.body.mailid)},${db.escape(hash)},'${db.escape(req.body.lastlogin)}')`,
                    (err, result) => {

                        if (err) {
                            return res.status(400).send({ msg: err });
                        }else {
                            return res.status(201).send({ msg: 'The user has been registerd with us!'});
                        }
                    });
                }
            });
        }
    });
});

router.post('/login', loginValidation, (req, res, next) => {

    db.query( `SELECT * FROM user WHERE mailid = ${db.escape(req.body.mailid)};`,
        (err, result) => {
            // user does not exists
            if (err) {
                //throw err;
                return res.status(400).send({
                   msg: err
                });
            }
                if (!result.length) {
                    return res.status(401).send({
                      msg: 'Email or password is incorrect'
                    });
                }
                // check password
            bcrypt.compare(req.body.password,result[0]['password'],(bErr, bResult) => {
                // wrong password
                if (bErr) {
                    //throw bErr;
                    return res.status(401).send({
                      msg: 'Email or password is incorrect!'
                    });
                }
                if (bResult) {
                    const token = jwt.sign({id:result[0].id},'the-super-strong-secrect',{ expiresIn: '1h' });
                    
                    db.query(`UPDATE user SET lastlogin = now() WHERE id = '${result[0].id}'`);
                    return res.status(200).send({
                        msg: 'Logged in!',
                        token,
                        user: result[0]
                    });
                }
                return res.status(401).send({
                  msg: 'Username or password is incorrect!'
                });  
            });
                
        }
    );
});

router.post('/get-user', signupValidation, (req, res, next) => {
    if(
        !req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]
    )
        {
            return res.status(422).json({
                message: "Please provide the token",
            });
        }
                const theToken = req.headers.authorization.split(' ')[1];
                const decoded = jwt.verify(theToken, 'the-super-strong-secrect');

        db.query('SELECT * FROM user where id=?', decoded.id, function (error, results, fields) {
            if (error) throw error;
            return res.send({ error: false, data: results[0], message: 'Fetch Successfully.' });
        });
});

router.get('/name',(req, res, next) => {
    db.query( "SELECT * FROM authors",function(err, result){
            // user does not exists
            if (err) {
                //throw err;
                return res.status(400).send({
                   msg: err
                });
            }
            //if (!result.length) {
                console.log(result);
                return res.status(200).send({
                    //result (null,res);
                  msg: 'get the data'
            
                })
            })
        });
router.post('/delete',(req, res, next) => {
    db.query( "select* FROM authors",req.body,function(err,req,res ,result){
        if (err) {
            //throw err;
            return res.status(400).send({
              msg: err
            });
        }
    })
});

db.query(`INSERT INTO authors(AuthorName,AuthorAge,AuthorDateofBirth,BooksName,BooksPublishedon,Booksprice) VALUES ('${db.escape( req.body.AuthorName)}','${db.escape( req.body.AuthorAge)}','${db.escape( req.body.AuthorDateofBirth)}','${db.escape(req.body.BooksName)}','${db.escape( req.body.BooksPublishedon)}','${db.escape( req.body.Booksprice)}')`,
                    (err, result) => {

                        if (err) {
                            return res.status(400).send({ msg: err });
                        }else {
                            return res.status(201).send({ msg: 'The user has been registerd with us!'});
                        }
                    });
                
module.exports = router;
